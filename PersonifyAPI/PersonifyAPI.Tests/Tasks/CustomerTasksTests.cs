﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PersonifyAPI.Tasks;


namespace PersonifyAPI.Tests.Tasks
{
    [TestClass]
    public class CustomerTasksTests
    {
        private static CustomerTasks _customerTasks;

        public CustomerTasksTests()
        {
            _customerTasks = new CustomerTasks();
        }

        [TestMethod]
        public void ReturnNextSequentialNumWithLeadingZeros_NumberWithLeadingZeros_ReturnsLeadingZeros()
        {
            var result = _customerTasks.ReturnNextSequentialNumWithLeadingZeros("D00311");
            Assert.AreEqual(result, "00312");
        }

        [TestMethod]
        public void ReturnNextSequentialNumWithLeadingZeros_NumberWithTrailingZeros_ReturnsTrailingZeros()
        {
            var result = _customerTasks.ReturnNextSequentialNumWithLeadingZeros("D11300");
            Assert.AreEqual(result, "11301");
        }

        [TestMethod]
        public void ReturnNextSequentialNumWithLeadingZeros_NumberWithLeadingAndTrailingZeros_ReturnsLeadingAndTrailingZeros()
        {
            var result = _customerTasks.ReturnNextSequentialNumWithLeadingZeros("D00300");
            Assert.AreEqual(result, "00301");
        }

        [TestMethod]
        public void ReturnNextSequentialNumWithLeadingZeros_NumberNoLeadingOrTrailingZeros_ReturnsNoLeadingOrTrailingZeros()
        {
            var result = _customerTasks.ReturnNextSequentialNumWithLeadingZeros("D11311");
            Assert.AreEqual(result, "11312");
        }
    }
}
