﻿namespace PersonifyAPI.Utility
{
    public static class MembershipClassCodes
    {
        public const string KeyClubClassCode = "KCCLUB";
        public const string CircleKClassCode = "CKCLUB";
        public const string AktionClubClassCode = "AKCLUB";

    }
}