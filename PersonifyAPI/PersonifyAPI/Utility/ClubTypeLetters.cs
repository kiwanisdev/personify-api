﻿namespace PersonifyAPI.Utility
{
    public static class ClubTypeLetters
    {
        public const string KeyClub = "H";
        public const string CircleK = "C";
        public const string AktionClub = "D";
    }
}