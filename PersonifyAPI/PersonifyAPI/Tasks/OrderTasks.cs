﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using PersonifyAPI.Exceptions;
using PersonifyAPI.Interfaces;
using PersonifyAPI.Models.Orders;
using TIMSS.API.AccountingInfo;
using TIMSS.API.CustomerInfo;
using TIMSS.API.OrderInfo;
using PersonifyAPI.Resources.Tasks;
using PersonifyAPI.Utility;
using TIMSS.API.Core.Validation;
using TIMSS.API.ProductInfo;

namespace PersonifyAPI.Tasks
{
    public class OrderTasks : IOrderTasks
    {
        private readonly int _fiscalYear;

        public OrderTasks()
        {
            _fiscalYear = DateTime.Now.Month > 9 ? DateTime.Now.Year : DateTime.Now.Year - 1;
        }

        public void MakePayment(MemberPaymentData paymentData)
        {
            var orders = BuildOrderDetails(paymentData.OrderNumber, null);
            ProcessPayment(orders, paymentData);
        }

        public string CreateCharterOrder(string masterCustomerId, CharterOrder order)
        {
            IList<OrderDetail> orderDetails = new List<OrderDetail>();

            try
            {
                CreateBasisHistory(masterCustomerId, order.ProductCode, order.NumberOfItems);
                var orderNumber = CreateOrder(masterCustomerId, order);
                orderDetails = BuildOrderDetails(orderNumber, null);

                return orderNumber;
            }
            catch (Exception)
            {
                DeleteOrder(orderDetails);
                throw;
            }
        }

        private string CreateOrder(string masterCustomerId, CharterOrder order)
        {
            var oMasters = (IOrderMasters)TIMSS.Global.GetCollection(OrderTasksResources.OrganizationId, OrderTasksResources.OrganizationUnitId, TIMSS.Enumerations.NamespaceEnum.OrderInfo, "OrderMasters");

            var productId = GetProductId(order.ProductCode);
            var oMast = oMasters.CreateNewOrder(masterCustomerId, 0, productId, "LIST", OrderTasksResources.OrderDetailStandardRateCode, null, 1, masterCustomerId);

            var od = (TIMSS.API.UserKIWANISTMAR.OrderInfo.OrderDetail)oMast.Details[0];

            od.Description = GetOrderDescriptions(order.ProductCode);

            od.RateCode.Code = OrderTasksResources.OrderDetailStandardRateCode;
            od.UserDefinedBasisCount = order.NumberOfItems;
            //od.BaseAdjustment = order.Amount.Total - od.BaseUnitPrice;

            if (order.Amount.Total > 0)
            {
                oMast.OrderStatusCodeString = OrderTasksResources.ActiveOrderStatusCode;
            }
            var orderNumber = oMast.OrderNumber;

            SaveOrderInfo(ref oMasters);

            return orderNumber;
        }

        private void CreateBasisHistory(string masterCustomerId, string productCode, int orderItemsCount)
        {
            //var basisHistories = (ICustomerBasisHistories)TIMSS.Global.GetCollection(OrderTasksResources.OrganizationId, OrderTasksResources.OrganizationUnitId, TIMSS.Enumerations.NamespaceEnum.CustomerInfo, "CustomerBasisHistories");
            var basisHistories = ( ICustomerBasisHistories)TIMSS.Global.GetCollection(OrderTasksResources.OrganizationId, OrderTasksResources.OrganizationUnitId, TIMSS.Enumerations.NamespaceEnum.CustomerInfo, "CustomerBasisHistories");
            var basisHistory = basisHistories.AddNew();

            var startDate = new DateTime(_fiscalYear, 10, 1);

            basisHistory.MasterCustomerId = masterCustomerId;
            basisHistory.SubCustomerId = 0;
            basisHistory.BasisCategoryCodeString = $"{productCode}CLUB";
            basisHistory.StartDate = startDate;
            basisHistory.OrganizationId = OrderTasksResources.OrganizationId;
            basisHistory.EndDate = new DateTime(_fiscalYear + 1, 9, 30);
            basisHistory.BasisTypeCodeString = OrderTasksResources.ValueBasisType;
            basisHistory.BasisValue = orderItemsCount;

            basisHistories.Validate();

            if (basisHistories.ValidationIssues.ErrorCount() > 0)
            {
                foreach (IIssue validationIssue in basisHistories.ValidationIssues)
                {
                    if (validationIssue.Message ==
                        "Changing Basis Value may change the existing rates for the membership detail lines and some or all Associate Individuals may become 'I'nactive. What do you want to do ?")
                    {
                        validationIssue.Responses.SelectedResponse = new IssueResponse(StandardResponseEnum.Yes);
                    }
                }
            }

            basisHistories.Save();

            if (basisHistories.ValidationIssues.ErrorCount() > 0)
            {

                string message = "";

                foreach (TIMSS.API.Core.Validation.IIssue issue in basisHistories.ValidationIssues)
                {
                    message += issue.Message + "|";
                }

                if (message != "")
                {
                    throw new PaymentException("Save order failed: " + message);
                }
            }


            SetBasisHistoryCount(masterCustomerId, startDate, orderItemsCount);

        }


        private static void SetBasisHistoryCount(string clubId, DateTime startDate, int count)
        {
            using (
                var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Personify"].ConnectionString)
            )
            {
                connection.Open();
                var cmd = new SqlCommand("kiwone_upd_cus_basis_history_sp", connection);
                cmd.Parameters.AddWithValue("@ClubKeyNumber", clubId);
                cmd.Parameters.AddWithValue("@StartDate", startDate);
                cmd.Parameters.AddWithValue("@BasisValue", count);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();

                connection.Close();
            }

        }

        private static IList<OrderDetail> BuildOrderDetails(string orderNumber, IList<int> existingDetails)
        {
            var details = new List<OrderDetail>();

            TIMSS.API.OrderInfo.IOrderDetails oDetails1;

            oDetails1 = ((TIMSS.API.OrderInfo.IOrderDetails)TIMSS.Global.GetCollection(OrderTasksResources.OrganizationId, OrderTasksResources.OrganizationUnitId, TIMSS.Enumerations.NamespaceEnum.OrderInfo, "OrderDetails"));
            oDetails1.Filter.Add("Order_No", orderNumber);
            oDetails1.Filter.Add("Line_Status_Code", TIMSS.Enumerations.QueryOperatorEnum.NotEqualTo, "C");
            oDetails1.Filter.Add("Package_Product_Id", "0");
            oDetails1.Fill();

            if (oDetails1.Count >= 1)
            {
                foreach (TIMSS.API.OrderInfo.IOrderDetail odet in oDetails1)
                {
                    if (existingDetails == null || !existingDetails.Contains(odet.OrderLineNumber))
                    {
                        details.Add(new OrderDetail
                        {
                            Code = odet.ProductCode,
                            RateCode = odet.RateCode.Code,
                            OrderNumber = orderNumber,
                            LineNumber = odet.OrderLineNumber,
                            AmountPaid = odet.ActualTotalAmount,
                            Quantity = (int)odet.OrderQuantity,
                            FirstName = odet.Badges.Count == 0 ? "" : odet.Badges[0].FirstName,
                            LastName = odet.Badges.Count == 0 ? "" : odet.Badges[0].LastName
                        });
                    }
                }
            }

            return details;
        }

        private int GetProductId(string productCode)
        {
            var products = (IProducts)TIMSS.Global.GetCollection(OrderTasksResources.OrganizationId, OrderTasksResources.OrganizationUnitId, TIMSS.Enumerations.NamespaceEnum.ProductInfo, "Products");
            products.Filter.Add("Product_code", productCode);
            products.Fill();

            return Convert.ToInt32(products[0].ProductId);
        }

        private static List<Tuple<string, int>> BuildOrdersAndLines(IEnumerable<OrderDetail> orderDetails)
        {
            var ordersAndLines = new List<Tuple<string, int>>();

            foreach (var detail in orderDetails)
            {
                ordersAndLines.Add(new Tuple<string, int>(detail.OrderNumber, detail.LineNumber));
            }

            return ordersAndLines;
        }

        private IARReceipts ProcessPayment(IList<OrderDetail> orderDetails, MemberPaymentData ccData)
        {
            var oAppOrgUnits = TIMSS.API.CachedApplicationData.ApplicationDataCache.get_ApplicationOrganizationUnits(OrderTasksResources.OrganizationId, OrderTasksResources.OrganizationUnitId);
            oAppOrgUnits[0].SetCurrentECommerceBatch();

            var oFarReceipts = (IARReceipts)TIMSS.Global.GetCollection(OrderTasksResources.OrganizationId, OrderTasksResources.OrganizationUnitId, TIMSS.Enumerations.NamespaceEnum.AccountingInfo, "ARReceipts");
            var oFarReceipt = oFarReceipts.AddNew();


            oFarReceipt.OrganizationUnitId = OrderTasksResources.OrganizationUnitId;
            oFarReceipt.OrganizationId = OrderTasksResources.OrganizationId;
            oFarReceipt.MasterCustomerId = ccData.MemberId;
            oFarReceipt.SubCustomerId = 0;
            oFarReceipt.BillMasterCustomerId = ccData.MemberId;
            oFarReceipt.BillSubCustomerId = 0;

            if (ccData.Amount.Total > 0)
            {
                oFarReceipt.Xrate = 1;
            }
            oFarReceipt.ReceiptTypeCode = oFarReceipt.ReceiptTypeCode.List[ccData.CreditCard.Type].ToCodeObject();
            oFarReceipt.CreditCardFlag = true;
            oFarReceipt.CCReferenceDecrypted = ccData.CreditCard.Number;
            oFarReceipt.CCExpirationDate = new DateTime(ccData.CreditCard.ExpirationYear, ccData.CreditCard.ExpirationMonth, 1);
            oFarReceipt.CCName = ccData.CreditCard.FirstName + " " + ccData.CreditCard.LastName;

            oFarReceipt.CCAddress1 = ccData.CreditCard.BillingAddress.Line1;
            oFarReceipt.CCCity = ccData.CreditCard.BillingAddress.City;
            oFarReceipt.CCState = ccData.CreditCard.BillingAddress.State;
            oFarReceipt.CCPostalCode = ccData.CreditCard.BillingAddress.PostalCode;
            oFarReceipt.CCCountryCode = ccData.CreditCard.BillingAddress.CountryCode;

            if (oFarReceipt.IsValid())
            {
                var ordersAndLines = BuildOrdersAndLines(orderDetails);
                oFarReceipt.AddOrderLineToPayWithReceipt(ordersAndLines.ToArray());
                oFarReceipt.ApplyCash(orderDetails.Sum(x => x.AmountPaid));
            }
            else
            {
                throw new Exception("OrderTasks/ProcessOrder: Receipt is invalid.");
            }

            if (oFarReceipt.PaymentAmount > 0)
            {
                var approved = oFarReceipt.AuthorizeCreditCard();
                if (!approved)
                {
                    // there is a bug in Personify that doesn't allow international orders if state and postal code are not set
                    oFarReceipt.CCState = OrderTasksResources.IndianaStateCode;
                    oFarReceipt.CCPostalCode = OrderTasksResources.IndianapolisZipCode;

                    approved = oFarReceipt.AuthorizeCreditCard();
                }

                if (!approved)
                {
                    throw new PaymentException("Credit card was not approved");
                }

                oFarReceipts.Save();
                var message = "";
                if (oFarReceipts.ValidationIssues.Count != 0)
                {
                    for (int i = 0; i < oFarReceipts.ValidationIssues.Count; i++)
                    {
                        message += "Validation Issue: " + oFarReceipts.ValidationIssues[i].Message + "|";
                    }

                    foreach (IARReceipt farReceipt in oFarReceipts)
                    {
                        if (!string.IsNullOrEmpty(farReceipt.CCRejectReason))
                        {
                            message += "Credit Card Rejection Reason: " + farReceipt.CCRejectReason + "|";
                        }
                    }

                    throw new PaymentException("Payment processing failed: " + message);
                }
            }

            return oFarReceipts;
        }

        private static void DeleteOrder(IList<OrderDetail> addedDetails)
        {
            var orderList = "";
            foreach (var orderNumber in addedDetails.Select(x => x.OrderNumber).Distinct())
            {
                orderList += !string.IsNullOrEmpty(orderList) ? "," + orderNumber : orderNumber;
                var oMasters = ((IOrderMasters)TIMSS.Global.GetCollection(OrderTasksResources.OrganizationId, OrderTasksResources.OrganizationUnitId, TIMSS.Enumerations.NamespaceEnum.OrderInfo, "OrderMasters"));
                oMasters.Filter.Add("Order_No", orderNumber);
                oMasters.Fill();

                if (oMasters.Count == 1)
                {
                    if (oMasters[0].OrderStatusCodeString == OrderTasksResources.ProFormaOrderStatusCode)
                    {
                        oMasters.RemoveAll();
                    }
                    else
                    {
                        var lines = addedDetails.Where(x => x.OrderNumber == orderNumber).Select(x => x.LineNumber);

                        for (int i = 0; i < oMasters[0].Details.Count; i++)
                        {
                            if (lines.Contains(oMasters[0].Details[i].OrderLineNumber))
                            {
                                oMasters[0].CancelOrder(oMasters[0].Details[i].OrderLineNumber);
                            }
                        }
                    }
                }

                oMasters.Validate();
                foreach (TIMSS.API.Core.Validation.IIssue i in oMasters.ValidationIssues)
                {
                    i.Responses.SelectedResponse = new TIMSS.API.Core.Validation.IssueResponse(TIMSS.API.Core.Validation.StandardResponseEnum.Yes);
                }
                oMasters.Save();

                var message = "";

                if (oMasters.ValidationIssues.Count > 0)
                {
                    foreach (TIMSS.API.Core.Validation.IIssue issue in oMasters.ValidationIssues)
                    {
                        message += issue.Message + "|";
                    }
                    throw new PaymentException(
                        string.Format("Order deletion failed. Order numbers are: {0}. Personify Message(s): {1}", orderList, message)
                        );
                }
            }
        }

        private static void SaveOrderInfo(ref TIMSS.API.OrderInfo.IOrderMasters oMasters)
        {
            string message = "";



            oMasters.Validate();
            if (oMasters.ValidationIssues.ErrorCount() > 0)
            {
                foreach (TIMSS.API.Core.Validation.IIssue i in oMasters.ValidationIssues)
                {
                    if (i.Message.Contains("Guest/Spouse") || i.Message.Contains("Youth") || i.Message.Contains("has already registered"))
                    {
                        i.Responses.SelectedResponse = new TIMSS.API.Core.Validation.IssueResponse(TIMSS.API.Core.Validation.StandardResponseEnum.Yes);
                    }
                    else if (i.Message.ToLower().Contains("bad address"))
                    {
                        i.Responses.SelectedResponse = new TIMSS.API.Core.Validation.IssueResponse(TIMSS.API.Core.Validation.StandardResponseEnum.Continue);
                    }
                    else
                    {
                        message += i.Message + "\n\r";
                    }
                }
                if (message != "")
                {
                    throw new PaymentException("Save order failed: " + message);
                }
            }

            oMasters.Save();
        }

        private string GetOrderDescriptions(string clubTypeCode)
        {
            switch (clubTypeCode)
            {
                case ClubTypeLetters.CircleK:
                    return OrderTasksResources.CkiClubCharterOrderDetailDescription;
                case ClubTypeLetters.AktionClub:
                    return OrderTasksResources.AktionClubCharterOrderDetailDescription;
                default:
                    return OrderTasksResources.KeyClubCharterOrderDetailDescription;
            }
        }


    }
}