﻿using System.Linq;
using PersonifyAPI.Interfaces;
using PersonifyAPI.Tasks.Dto.District;
using TIMSS.API.UserKIWANISTMAR.CustomerInfo;
using TIMSS.Enumerations;

namespace PersonifyAPI.Tasks
{
    public class DistrictTasks : IDistrictTasks
    {
        public DistrictWithTier[] GetCkiDistricts()
        {
            var districts = GetDistricts("CKDIST");
            return districts;
        }

        public DistrictWithTier[] GetKeyClubDistricts()
        {
            var districts = GetDistricts("KCDIST");
            return districts;
        }

        public DistrictWithTier[] GetAktionClubDistricts()
        {
            var districts = GetDistricts("AKDIST");
            return districts;
        }


        private static DistrictWithTier[] GetDistricts(string membershipClassCode)
        {
            var customers = (TIMSS.API.UserKIWANISTMAR.CustomerInfo.Customers)TIMSS.Global.GetCollection("KIWANIS", "KI", NamespaceEnum.CustomerInfo, "Customers");
            customers.Filter.Add("USR_MEMBERSHIP_CLASS_CODE", membershipClassCode);
            customers.Filter.Add("USR_MEMBERSHIP_STATUS_CODE", QueryOperatorEnum.NotEqualTo, "CR");
            customers.Filter.Add("USR_MEMBERSHIP_STATUS_CODE", QueryOperatorEnum.NotEqualTo, "CSD");


            customers.Fill();

            var districts = (
                from Customer customer in customers
                orderby customer.MasterCustomerId
                select new DistrictWithTier
                {
                    DistrictId = customer.MasterCustomerId,
                    DistrictName = customer.LabelName,
                    Tier = customer.UserDefinedTierString
                }).ToArray();

            return districts;
        }

    }
}