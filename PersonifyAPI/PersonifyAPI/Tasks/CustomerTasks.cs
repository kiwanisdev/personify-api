﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using PersonifyAPI.Interfaces;
using PersonifyAPI.Models;
using PersonifyAPI.Models.Customer;
using TIMSS.API.Base.CustomerInfo;
using TIMSS.Enumerations;
using Customer = TIMSS.API.UserKIWANISTMAR.CustomerInfo.Customer;
using CustomerRelationship = TIMSS.API.UserKIWANISTMAR.CustomerInfo.CustomerRelationship;
using Customers = TIMSS.API.UserKIWANISTMAR.CustomerInfo.Customers;
using PersonifyAPI.Resources.Tasks;
using PersonifyAPI.Utility;

namespace PersonifyAPI.Tasks
{
    public class CustomerTasks : ICustomerTasks
    {
        private readonly IOrderTasks _orderTasks;
        private readonly int _fiscalYear;

        public CustomerTasks() : this(new OrderTasks())
        {
        }

        public CustomerTasks(IOrderTasks orderTasks)
        {
            _orderTasks = orderTasks;
            _fiscalYear = DateTime.Now.Month > 9 ? DateTime.Now.Year : DateTime.Now.Year - 1;
        }

        public CharterInfo CreateCharter(NewCharter charter)
        {
            var newClubKeyNumber = CreateClub(charter);
            var orderNumber = _orderTasks.CreateCharterOrder(newClubKeyNumber, charter.CharterOrder);

            return new CharterInfo
            {
                MasterCustomerId = newClubKeyNumber,
                OrderNumber = orderNumber
            };
        }

        private string CreateClub(NewCharter charter)
        {
            string keyNum = GenerateNewClubKeyNumber(charter.NewClubType);

            Customers oCustomers = (Customers)TIMSS.Global.GetCollection(OrderTasksResources.OrganizationId, OrderTasksResources.OrganizationUnitId, NamespaceEnum.CustomerInfo, "Customers");

            Customer cus = (Customer)oCustomers.AddNew();
            cus.LastName = charter.NewClubName;
            cus.LastFirstName = charter.NewClubName;
            cus.LabelName = charter.NewClubName;
            cus.MasterCustomerId = keyNum;
            cus.OrganizationId = OrderTasksResources.OrganizationId;
            cus.OrganizationUnitId = OrderTasksResources.OrganizationUnitId;
            cus.RecordType = CustomerTasksResources.CompanyRecordTypeCode;
            cus.CanPlaceOrderFlag = true;
            cus.CustomerClassCodeString = CustomerTasksResources.CompanyClassCode;
            cus.CustomerStatusCodeString = CustomerTasksResources.ActiveStatusCode;
            cus.UserDefinedMembershipClassCodeString = $"{GetClubKeyLetter(charter.NewClubType)}IB";
            cus.AllowFaxFlag = true;
            cus.AllowEmailFlag = true;
            cus.AllowPhoneFlag = true;
            cus.AllowLabelSalesFlag = true;
            cus.AllowSolicitationFlag = true;
            cus.AllowInternalMailFlag = true;
            cus.TaxableFlag = false;
            cus.EnforceCommitteeStructureFlag = false;
            cus.NotADuplicateFlag = false;
            cus.ExhibitorFlag = false;
            cus.SubCustomerId = 0;
            cus.UserDefinedMembershipStatusChangeDate = DateTime.Now;
            cus.UserDefinedDistrictStatusChangeDate = DateTime.Now;
            cus.NewRecord = true;
            cus.UserDefinedClubClassificationString = charter.NewClubType;
            cus.UserDefinedGeographicAreaString = GetGeographicCode(charter.NewClubCountry);
            cus.UserDefinedStateCodeString = !string.IsNullOrEmpty(charter.NewClubState) ? charter.NewClubState : "";
            cus.UserDefinedOfficialCity = charter.NewClubCity;
            cus.UserDefinedCountryCodeString = charter.NewClubCountry;
            cus.UserDefinedCurrencyCodeString = OrderTasksResources.USDCurrencyCode;

            AddAddress(cus, charter);

            if(charter.SponsoringClub != null)
                AddRelationship(cus, charter.SponsoringClub);

            if (charter.SponsoringAdvisor != null)
            {
                if(charter.SponsoringAdvisor.MasterCustomerId != "")
                    AddRelationship(cus, charter.SponsoringAdvisor, isAdvisorRelationship: true);
            }
            
            if (oCustomers.Save())
                return keyNum;

            string message = "";
            if (oCustomers.ValidationIssues.ErrorCount() > 0)
            {
                foreach (TIMSS.API.Core.Validation.IIssue i in oCustomers.ValidationIssues)
                {
                    if (!string.IsNullOrEmpty(message))
                    {
                        message += ", ";
                    }

                    message += i.Message;

                    i.Responses.SelectedResponse = new TIMSS.API.Core.Validation.IssueResponse(TIMSS.API.Core.Validation.StandardResponseEnum.Yes);
                }
            }

            if (oCustomers.Save())
                return keyNum;

            throw new Exception(message);
        }

        private void AddAddress(Customer cus, NewCharter charter)
        {
            CustomerAddressDetail cad = (CustomerAddressDetail)cus.CreateDefaultAddress();
            cad.AddressTypeCodeString = CustomerTasksResources.ClubAddressTypeCode;
            cad.AddressStatusCodeString = CustomerTasksResources.GoodAddressStatusCode;
            cad.CustomerAddressId = cad.Address.CustomerAddressId;
            cad.OneTimeUseFlag = false;
            cad.APFlag = true;
            cad.PrioritySeq = 0;
            cad.ShipToFlag = true;
            cad.BillToFlag = true;
            cad.ConfidentialFlag = false;
            cad.DirectoryFlag = true;
            cad.DirectoryPriority = 0;
            cad.RecurFlag = false;
            cad.Address.CountryCode.Code = charter.NewClubCountry;
            cad.Address.Address1 = charter.NewClubStreetAddress;
            cad.Address.City = charter.NewClubCity;
            cad.Address.State = charter.NewClubState;
            cad.Address.PostalCode = charter.NewClubPostalCode;
            cad.Address.AddressStatusCodeString = CustomerTasksResources.GoodAddressStatusCode;
            cad.Address.IsAddressValidationAutoRespond = true;
            cad.Address.IsDefaultCountryCode = true;
        }

        private void AddRelationship(Customer cus, Sponsor sponsor, bool isAdvisorRelationship = false)
        {
            CustomerRelationship relationship = (CustomerRelationship)cus.Relationships.AddNew();
            relationship.RelatedName = sponsor.NameIdentifier;
            relationship.RelationshipTypeString = CustomerTasksResources.AdvisorRelationshipType;
            relationship.RelationshipCodeString = isAdvisorRelationship ? CustomerTasksResources.AdvisorRelationshipCode : CustomerTasksResources.ClubRelationshipCode;
            relationship.ReciprocalCodeString = isAdvisorRelationship ? CustomerTasksResources.ClubRelationshipCode : CustomerTasksResources.SponsorRelationshipCode;
            relationship.RelatedMasterCustomerId = sponsor.MasterCustomerId;
            relationship.RelatedSubCustomerId = 0;
            relationship.BeginDate = DateTime.Now;
            relationship.UserDefinedDistrictStatusChangeDate = DateTime.Now;
            relationship.UserDefinedMembershipStatusChangeDate = DateTime.Now;
            relationship.FullTimeFlag = false;
            relationship.PrimaryEmployerFlag = true;
        }

        private string GetGeographicCode(string countryCode)
        {
            switch (countryCode)
            {
                case "USA":
                    return "US";
                case "CAN":
                    return "CAN";
                case "MEX":
                    return "LA";
                case "PAN":
                    return "LA";
                case "NIC":
                    return "LA";
                case "SLV":
                    return "LA";
                case "HND":
                    return "LA";
                case "GTM":
                    return "LA";
                case "CRI":
                    return "LA";
                case "BLZ":
                    return "LA";
            }

            string geoArea = string.Empty;

            TIMSS.API.Core.SearchObject so = new TIMSS.API.Core.SearchObject("KIWANIS", "KI");
            so.Target = TIMSS.Global.GetCollection("KIWANIS", "KI", NamespaceEnum.ApplicationInfo,
                                                   "ApplicationCountries");

            TIMSS.API.Core.SearchProperty p = new TIMSS.API.Core.SearchProperty();

            p = new TIMSS.API.Core.SearchProperty("CountryCode")
            {
                Caption = "CountryCode",
                Operator = QueryOperatorEnum.Equals,
                UseInQuery = true,
                ShowInQuery = false,
                ShowInResults = true,
                Value = countryCode
            };
            so.Parameters.Add(p);

            p = new TIMSS.API.Core.SearchProperty("ContinentCode") { Caption = "ContinentCode", UseInQuery = false, ShowInQuery = false, ShowInResults = true };
            so.Parameters.Add(p);

            so.Search();

            DataTable results = so.Results.ToTable();

            if (results.Rows.Count > 0)
            {
                geoArea = results.Rows[0].Field<string>("ContinentCode");
            }

            switch (geoArea)
            {
                case "AF":
                    return "AFR";
                case "AS":
                    return "APA";
                case "OC":
                    return "APA";
                case "EU":
                    return "EUR";
                case "SA":
                    return "LA";
                case "NA":
                    return "CAR";
            }
            return geoArea;
        }

        private string GenerateNewClubKeyNumber(string clubType)
        {
            string maxClubKeyNumber = GetMaxClubKeyNumber(clubType);

            var numWithLeadingZeros = ReturnNextSequentialNumWithLeadingZeros(maxClubKeyNumber);

            return $"{GetClubKeyLetter(clubType)}{numWithLeadingZeros}";
        }

        public string ReturnNextSequentialNumWithLeadingZeros(string maxClubNumber)
        {
            if (int.TryParse(maxClubNumber.Substring(1), out var num))
                num++;
            else
                throw new Exception("CustomerTasks/GenerateNewClubKeyNumber - Unable to generate new club key number.");

            var numWithLeadingZeros = num.ToString("D5");
            return numWithLeadingZeros;
        }

        private string GetMaxClubKeyNumber(string clubType)
        {
            string mostRecentClubNumber;

            using (
                var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Personify"].ConnectionString)
            )
            {
                var sql = @"SELECT TOP 1 MASTER_CUSTOMER_ID 
                            FROM CUSTOMER
                            where USR_MEMBERSHIP_CLASS_CODE IN(" +
                            GetClubClassCodes(clubType) +
                            @")
                            AND MASTER_CUSTOMER_ID NOT IN('K-EURSTAFF', 'K-STAFF', 'KE-STAFF')
                            and MASTER_CUSTOMER_ID like '[BCDHKP][0-9][0-9][0-9][0-9][0-9]' 
                            order by MASTER_CUSTOMER_ID desc";

                connection.Open();
                var cmd = new SqlCommand(sql, connection) { CommandType = CommandType.Text };
                var result = cmd.ExecuteScalar();

                connection.Close();

                mostRecentClubNumber = result != null ? result.ToString() : "";
            }

            return mostRecentClubNumber;

        }

        private string GetClubClassCodes(string clubType)
        {
            switch (clubType)
            {
                case MembershipClassCodes.CircleKClassCode:
                    return CustomerTasksResources.CircleKClassCodes;
                case MembershipClassCodes.KeyClubClassCode:
                    return CustomerTasksResources.KeyClubClassCodes;
                case MembershipClassCodes.AktionClubClassCode:
                    return CustomerTasksResources.AktionClubClassCodes;
                default:
                    return null;
            }
        }

        private string GetClubKeyLetter(string clubType)
        {
            switch (clubType)
            {
                case MembershipClassCodes.CircleKClassCode:
                    return ClubTypeLetters.CircleK;
                case MembershipClassCodes.KeyClubClassCode:
                    return ClubTypeLetters.KeyClub;
                case MembershipClassCodes.AktionClubClassCode:
                    return ClubTypeLetters.AktionClub;
                default:
                    return null;
            }
        }
    }
}