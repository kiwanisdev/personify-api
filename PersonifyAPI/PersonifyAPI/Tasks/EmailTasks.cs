﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using Newtonsoft.Json;
using NLog;
using PersonifyAPI.Models.Customer;


namespace PersonifyAPI.Tasks
{
    public class EmailTasks
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        internal static void SendErrorAlertMessage(Exception exception, NewCharter charter)
        {
            var errorMailMessage = new MailMessage
            {
                Subject = "SLP Chartering: Error processing the new charter application",
                IsBodyHtml = true
            };

            errorMailMessage.To.Add(ConfigurationManager.AppSettings["WebDevEmailAddress"]); 
           
            errorMailMessage.Body = $"Error processing the new charter application : {exception}"+
                                     $"Charter attempted to process following request: {JsonConvert.SerializeObject(charter)}";

            SendEmail(errorMailMessage);

        }


        private static void SendEmail(MailMessage mail)
        {
            try
            {
                var smtp = ConstructSmtpClient();

                mail.From = new MailAddress(ConfigurationManager.AppSettings["KiwanisFromEmailAddress"]);
               
                smtp.Send(mail);
            }
            catch (SmtpException e)
            {
                _logger.Error(e, "Email failed to send");
            }

            catch (Exception e)
            {
                _logger.Error(e, "Email failed to send");
            }
        }



        private static SmtpClient ConstructSmtpClient()
        {
            return new SmtpClient(ConfigurationManager.AppSettings["SMTPConnection"],
                Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]))
            {
                Credentials =
                    new NetworkCredential(ConfigurationManager.AppSettings["SMTPId"],
                        ConfigurationManager.AppSettings["SMTPKey"]),
                EnableSsl = true
            };
        }

    }
}