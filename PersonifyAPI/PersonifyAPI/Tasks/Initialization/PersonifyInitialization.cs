﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonifyAPI.Tasks.Initialization
{
    public class PersonifyInitialization
    {
        public static void Init()
        {
            TIMSS.PersonifyInitializer.Initialize("..\\Config", new TIMSS.Client.Implementation.Security.Authentication.ImmediateSeatInformationProvider());
        }

        // We need this function and referenced variables inside to be sure that 
        // "TIMSS.Server.UniversalPaymentHandler.dll" gets included into compilation output. That DLL
        // is used by Personify, but VS doesn't always copy it because it can't detect direct 
        // classes usage and thus skips the DLL due to optimization. So we add an obvious call to any 
        // routine from "TIMSS.Server.UniversalPaymentHandler.dll" and by doing this Visual Studio
        // gets assured that the DLL is used and needs to be copied.
        public static void ReferencePersonifyAssemblies()
        {
            new TIMSS.Server.UniversalPaymentHandler.CustomerProfile();

        }
    }
}