﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PersonifyAPI.Interfaces;
using PersonifyAPI.Tasks.Dto.Country;
using TIMSS.API.ApplicationInfo;
using TIMSS.Enumerations;

namespace PersonifyAPI.Tasks
{
    public class CountryTasks : ICountryTasks
    {
        public IEnumerable<SelectListItem> GetCountryCodes(string countryCode)
        {
            var countryList = new List<SelectListItem>();
            var countries = (IApplicationCountries) TIMSS.Global.GetCollection("KIWANIS", "KI",
                NamespaceEnum.ApplicationInfo, "ApplicationCountries");
            countries.Filter.Add("ACTIVE_FLAG", "Y");
            countries.Fill();

            if (countries.Count > 0)
            {
                foreach (IApplicationCountry country in countries)
                {
                    countryList.Add(new SelectListItem
                    {
                        Value = country.CountryCode,
                        Text = country.CountryDescription,
                        Selected = country.CountryCode == (countryCode ?? "USA")
                    });
                }
            }

            return countryList.OrderBy(c => c.Text);
        }

        public List<StateCodeWithTitle> GetStateCodesAndNames(string countryCode)
        {
            if (string.IsNullOrWhiteSpace(countryCode))
                throw new ArgumentNullException(nameof(countryCode));

            var states = (IApplicationStates) TIMSS.Global.GetCollection(
                "KIWANIS",
                "KI",
                NamespaceEnum.ApplicationInfo,
                "ApplicationStates"
            );

            states.Filter.Add("COUNTRY_CODE", countryCode);
            states.Filter.Add("ACTIVE_FLAG", "Y");
            states.Sort("StateDescription");
            states.Fill();

            var result =
                states.Count > 0
                    ? states.Cast<IApplicationState>()
                        .Select(x => new StateCodeWithTitle(x.StateCode, x.StateDescription))
                        .OrderBy(x => x.Name)
                        .ToList()
                    : new List<StateCodeWithTitle>();

            return result;
        }
    }
}