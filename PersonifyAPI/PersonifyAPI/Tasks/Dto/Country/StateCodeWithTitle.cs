﻿namespace PersonifyAPI.Tasks.Dto.Country
{
    public class StateCodeWithTitle
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public StateCodeWithTitle()
        {

        }

        public StateCodeWithTitle(string code, string name)
        {
            Code = code;
            Name = name;
        }
    }
}