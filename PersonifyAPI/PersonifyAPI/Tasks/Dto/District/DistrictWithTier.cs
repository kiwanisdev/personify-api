﻿namespace PersonifyAPI.Tasks.Dto.District
{
    public class DistrictWithTier
    {
        public string DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string Tier { get; set; }
    }
}