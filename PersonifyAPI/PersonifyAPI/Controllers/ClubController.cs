﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using NLog;
using PersonifyAPI.Interfaces;
using PersonifyAPI.Models.Customer;
using PersonifyAPI.Models.Orders;
using PersonifyAPI.Tasks;

namespace PersonifyAPI.Controllers
{
    public class ClubController : ApiController
    {
        private readonly ICustomerTasks _customerTasks;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public ClubController() : this(new CustomerTasks())
        {
        }

        public ClubController(ICustomerTasks customerTasks)
        {
            _customerTasks = customerTasks;
        }

        [HttpPost]
        public HttpResponseMessage CreateCharter(NewCharter charter)
        {
            try
            {
                var result = Request.CreateResponse(
                    HttpStatusCode.OK,
                    _customerTasks.CreateCharter(charter)
                );

                return result;
            }
            catch (Exception e)
            {
               
                EmailTasks.SendErrorAlertMessage(e, charter);
                _logger.Error(e, JsonConvert.SerializeObject(charter) );

                var errorResponseData = new CreateCharterErrorResponse
                {
                    Message = e.Message
                };

                var errorResponse = Request.CreateResponse(HttpStatusCode.InternalServerError, errorResponseData);

                return errorResponse;
            }
        }
    }
}