﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PersonifyAPI.Exceptions;
using PersonifyAPI.Interfaces;
using PersonifyAPI.Models.Orders;
using PersonifyAPI.Tasks;

namespace PersonifyAPI.Controllers
{
    public class OrderController : ApiController
    {
        private readonly IOrderTasks _orderTasks;

        public OrderController() : this(new OrderTasks())
        {
        }

        public OrderController(IOrderTasks orderTasks)
        {
            _orderTasks = orderTasks;
        }

        [HttpPost]
        public IHttpActionResult PayOrder(PaymentRequest request)
        {
            var paymentData = new MemberPaymentData
            {
                MemberId = request.MemberMasterCustomerId,
                ProductCode = request.ProductCode,
                OrderNumber = request.OrderNumber,
                LineNum = 1,
                CreditCard = request.CreditCard,
                Amount = new Amount
                {
                    Total = request.Amount.Total,
                    CurrencyCode = request.Amount.Currency
                }
            };

            try
            {
                _orderTasks.MakePayment(paymentData);
                return Ok();
            }
            catch (PaymentException e)
            {
                var errorResponse = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(e.Message, System.Text.Encoding.UTF8, "text/plain"),
                    StatusCode = HttpStatusCode.BadRequest
                };

                throw new HttpResponseException(errorResponse);
            }
            catch (Exception e)
            {
                var errorResponse = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message, System.Text.Encoding.UTF8, "text/plain"),
                    StatusCode = HttpStatusCode.InternalServerError
                };

                throw new HttpResponseException(errorResponse);
            }
        }
    }
}