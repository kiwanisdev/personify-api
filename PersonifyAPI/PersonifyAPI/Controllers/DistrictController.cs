﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using PersonifyAPI.Interfaces;
using PersonifyAPI.Models.District;
using PersonifyAPI.Tasks;
using PersonifyAPI.Tasks.Dto.District;

namespace PersonifyAPI.Controllers
{
    [RoutePrefix("api/district")]
    public class DistrictController : ApiController
    {

        private readonly IDistrictTasks _districtTasks;

        public DistrictController()
            : this(new DistrictTasks())
        {

        }

        public DistrictController(IDistrictTasks districtTasks)
        {
            _districtTasks = districtTasks;
        }


        [HttpGet]
        [Route("GetDistricts")]
        public IHttpActionResult GetDistricts()
        {
            var response = new GetDistrictsResponse
            {
                AktionClubDistricts = MapDistricts(_districtTasks.GetAktionClubDistricts()),
                CkiDistricts = MapDistricts(_districtTasks.GetCkiDistricts()),
                KeyClubDistricts = MapDistricts(_districtTasks.GetKeyClubDistricts())
            };

            return Ok(response);
        }

        private static DistrictInfo[] MapDistricts(IEnumerable<DistrictWithTier> districts)
        {
            if (districts == null)
            {
                return null;
            }

            var result =
                districts
                    .Select(MapDistrict)
                    .ToArray();

            return result;
        }

        private static DistrictInfo MapDistrict(DistrictWithTier district)
        {
            if (district == null)
            {
                return null;
            }

            return new DistrictInfo
            {
                DistrictKeyNumber = district.DistrictId,
                DistrictName = district.DistrictName,
                Tier = district.Tier
            };
        }
    }
}