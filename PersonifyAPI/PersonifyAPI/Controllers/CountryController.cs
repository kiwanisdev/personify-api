﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using PersonifyAPI.Interfaces;
using PersonifyAPI.Models.Country;
using PersonifyAPI.Tasks;

namespace PersonifyAPI.Controllers
{
    [RoutePrefix("api/country")]
    public class CountryController : ApiController
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly ICountryTasks _countryTasks;

        public CountryController():this(new CountryTasks())
        {
        }

        public CountryController(ICountryTasks countryTasks)
        {
            _countryTasks = countryTasks;
        }

        [HttpGet]
        public IHttpActionResult GetCountries(string countryCode)
        {
            try
            {
                return Ok(_countryTasks.GetCountryCodes(countryCode));
            }
            catch (Exception e)
            {
                var errorResponse = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message, System.Text.Encoding.UTF8, "text/plain"),
                    StatusCode = HttpStatusCode.InternalServerError
                };

                throw new HttpResponseException(errorResponse);
            }
        }


        [HttpGet]
        [System.Web.Http.Route("states")]
        public IHttpActionResult GetStatesByCountry(string countryCode)
        {
            if (string.IsNullOrEmpty(countryCode))
            {
                ModelState.AddModelError(nameof(countryCode), "Country code is required");
                return BadRequest(ModelState);
            }

            var states = _countryTasks.GetStateCodesAndNames(countryCode);
            var response = new GetStatesByCountryResponse
            {
                States = states.Select(x => new StateModel {Code = x.Code, Name = x.Name}).ToArray()
            };

            return Ok(response);
        }
    }
}