﻿using PersonifyAPI.Models.Orders;

namespace PersonifyAPI.Interfaces
{
    public interface IOrderTasks
    {
        void MakePayment(MemberPaymentData paymentData);

        string CreateCharterOrder(string masterCustomerId, CharterOrder order);

    }
}
