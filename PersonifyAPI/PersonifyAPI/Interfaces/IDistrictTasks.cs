﻿using PersonifyAPI.Tasks.Dto.District;

namespace PersonifyAPI.Interfaces
{
    public interface IDistrictTasks
    {
        DistrictWithTier[] GetCkiDistricts();
        DistrictWithTier[] GetKeyClubDistricts();
        DistrictWithTier[] GetAktionClubDistricts();
    }
}