﻿using System.Collections.Generic;
using System.Web.Mvc;
using PersonifyAPI.Tasks.Dto.Country;

namespace PersonifyAPI.Interfaces
{
    public interface ICountryTasks
    {
        IEnumerable<SelectListItem> GetCountryCodes(string countryCode);

        List<StateCodeWithTitle> GetStateCodesAndNames(string countryCode);
    }
}