﻿using PersonifyAPI.Models;
using PersonifyAPI.Models.Customer;

namespace PersonifyAPI.Interfaces
{
    public interface ICustomerTasks
    {
        CharterInfo CreateCharter(NewCharter charter);
    }
}