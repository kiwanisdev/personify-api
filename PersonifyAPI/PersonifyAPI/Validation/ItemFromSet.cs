﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PersonifyAPI.Validation
{
    public class ItemFromSet : ValidationAttribute
    {

        public string[] AllowedItems { get; }

        public bool IgnoreCase { get; set; }

        public ItemFromSet(string[] allowedItems)
        {
            AllowedItems = allowedItems;
        }

        public override bool IsValid(object value)
        {
            if (value != null)
            {
                if (!(value is string))
                {
                    return false;
                }

                if (AllowedItems == null)
                {
                    return false;
                }

                var strValue = (string)value;

                if (IgnoreCase)
                {
                    strValue = strValue?.ToLower();
                    if (AllowedItems.All(x => x?.ToLower() != strValue))
                    {
                        return false;
                    }
                }
                else
                {
                    if (!AllowedItems.Contains(strValue))
                    {
                        return false;
                    }
                }

            }

            return true;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(
                "The field {0} is invalid. Permitted values are: {1}.{2}"
                , name
                , AllowedItems == null || AllowedItems.Length == 0 
                    ? "()" 
                    : string.Join(", ", AllowedItems)
                , IgnoreCase ? " Letters are not case sensitive." : ""
                );
        }
    }
}