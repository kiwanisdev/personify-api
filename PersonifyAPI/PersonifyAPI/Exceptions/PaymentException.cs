﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PersonifyAPI.Exceptions
{
    public class PaymentException : ApplicationException
    {
        public PaymentException()
        {
        }

        public PaymentException(string message) : base(message)
        {
        }

        public PaymentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PaymentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}