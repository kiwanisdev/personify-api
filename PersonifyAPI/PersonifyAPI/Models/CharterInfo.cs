﻿namespace PersonifyAPI.Models
{
    public class CharterInfo
    {
        public string MasterCustomerId { get; set; }
        public string OrderNumber { get; set; }
    }
}