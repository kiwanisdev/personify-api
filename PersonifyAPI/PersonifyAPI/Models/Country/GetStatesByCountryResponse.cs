﻿namespace PersonifyAPI.Models.Country
{
    public class GetStatesByCountryResponse
    {
        public StateModel[] States { get; set; }
    }
}