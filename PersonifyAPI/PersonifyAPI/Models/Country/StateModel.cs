﻿namespace PersonifyAPI.Models.Country
{
    public class StateModel
    {
        public string Code { get; set; }
        public string Name { get; set; }

    }
}