﻿using PersonifyAPI.Models.Orders;

namespace PersonifyAPI.Models.Customer
{
    public class NewCharter
    {
        public string NewClubName { get; set; }
        public string NewClubType { get; set; }
        public string NewClubCountry { get; set; }
        public string NewClubState { get; set; }
        public string NewClubCity { get; set; }
        public string NewClubStreetAddress { get; set; }
        public string NewClubPostalCode { get; set; }
        public Sponsor SponsoringClub { get; set; }
        public Sponsor SponsoringAdvisor { get; set; }
        public CharterOrder CharterOrder { get; set; }
    }
}