﻿namespace PersonifyAPI.Models.Customer
{
    public class Sponsor
    {
        public string NameIdentifier { get; set; }
        public string MasterCustomerId { get; set; }
    }
}