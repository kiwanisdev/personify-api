﻿namespace PersonifyAPI.Models.District
{
    public class DistrictInfo
    {
        public string DistrictKeyNumber { get; set; }
        public string DistrictName { get; set; }
        public string Tier { get; set; }
    }
}