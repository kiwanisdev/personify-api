﻿namespace PersonifyAPI.Models.District
{
    public class GetDistrictsResponse
    {
        public DistrictInfo[] CkiDistricts { get; set; }
        public DistrictInfo[] KeyClubDistricts { get; set; }
        public DistrictInfo[] AktionClubDistricts { get; set; }
    }
}