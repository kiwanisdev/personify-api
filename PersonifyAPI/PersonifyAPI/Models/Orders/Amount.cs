﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonifyAPI.Models.Orders
{
    public class Amount
    {
        public string CurrencyCode { get; set; }
        public decimal Total { get; set; }
    }
}