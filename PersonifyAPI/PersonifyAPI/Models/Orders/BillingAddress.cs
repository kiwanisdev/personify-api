﻿using System.ComponentModel.DataAnnotations;

namespace PersonifyAPI.Models.Orders
{
    public class BillingAddress
    {
        /// <summary>
        /// Address line 1
        /// </summary>
        [Required]
        public string Line1 { get; set; } // "111 First Street",

        /// <summary>
        /// City
        /// </summary>
        [Required]
        public string City { get; set; } // "Saratoga",

        /// <summary>
        /// State/province
        /// </summary>
        [Required]
        public string State { get; set; } // "CA",

        /// <summary>
        /// Postal code
        /// </summary>
        [Required]
        public string PostalCode { get; set; } // "95070",

        /// <summary>
        /// 2-digits country code as per ISO 3166-1 alpha-2
        /// </summary>
        [Required]
        public string CountryCode { get; set; } // "US"

    }
}