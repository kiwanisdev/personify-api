﻿using System.ComponentModel.DataAnnotations;
using PersonifyAPI.Validation;

namespace PersonifyAPI.Models.Orders
{
    public class CreditCardInfo
    {
        /// <summary>
        /// Credit card number
        /// </summary>
        [Required]
        public string Number { get; set; } // "4417119669820331",

        /// <summary>
        /// Type of credit card: ("VISA", "MASTER_CARD" etc.)
        ///
        /// AMEX - American Express;												
        /// CARTA_SI - Carta Si;
        /// CARTE_BLANCHE - Carte Blanche;                                          
        /// CARTE_BLEUE - Carte Bleue;                                             
        /// DANKORT Dankort;                                                      
        /// DELTA - Delta-Global Payment Service only. Others Use VISA;
        /// DINERS_CLUB - Diners Club US &amp; Canada;                                  
        /// DISCOVER - Discover;                                                    
        /// ENCODED_ACCOUNT - Encoded Account Number;                               
        /// ENROUTE - enRoute;                                                      
        /// GE_MONEY - GE Money UK card;                                            
        /// JAL - JAL;                                                              
        /// JCB - JCB;                                                              
        /// LASER Laser                                                          
        /// MAESTRO - Maestro (UK Domestic)/Solo;                                   
        /// MAESTRO_INTERNATIONAL - Maestro International;                          
        /// MASTER_CARD - MasterCard/Eurocard;                                      
        /// SOLO - Solo-GlobalPayment Service only.Others Use MAESTRO;              
        /// UATP - UATP;                                                            
        /// VISA - Visa;                                                            
        /// VISA_ELECTRON - Visa Electron											
        /// </summary>
        [Required]
        [ItemFromSet(new []
        {
            "AMEX", "CARTA_SI", "CARTE_BLANCHE", "CARTE_BLEUE", "DANKORT", "DELTA", "DINERS_CLUB", "DISCOVER",
            "ENCODED_ACCOUNT", "ENROUTE", "GE_MONEY", "JAL", "JCB", "LASER", "MAESTRO", "MAESTRO_INTERNATIONAL",
            "MASTERCARD", "SOLO", "UATP", "VISA", "VISA_ELECTRON"
        },
            ErrorMessage = "Unknown type of credit card"
        )]
        public string Type { get; set; } //  "visa",

        /// <summary>
        /// Expiration date - month
        /// </summary>
        [Required]
        public int ExpirationMonth { get; set; } //  11,

        /// <summary>
        /// Expiration date - year in 4-digits format (i.e. 2018)
        /// </summary>
        [Required]
        public int ExpirationYear { get; set; } //  2018,

        /// <summary>
        /// CVV2 code from back of credit card
        /// </summary>
        [Required]
        public string CVV { get; set; } //  "874",

        /// <summary>
        /// Credit card owner first name
        /// </summary>
        [Required]
        public string FirstName { get; set; } // "Betsy",

        /// <summary>
        /// Credit card owner last name
        /// </summary>
        [Required]
        public string LastName { get; set; } // "Buyer",

        /// <summary>
        /// Credit card owner's billing address
        /// </summary>
        [Required]
        public BillingAddress BillingAddress { get; set; }

    }
}