﻿namespace PersonifyAPI.Models.Orders
{
    public class CharterOrder
    {
        public string ProductCode { get; set; }
        public int NumberOfItems { get; set; }
        public Amount Amount { get; set; }
    }
}