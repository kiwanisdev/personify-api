﻿namespace PersonifyAPI.Models.Orders
{
    public class CreateCharterErrorResponse
    {
        public string Message { get; set; }
    }
}