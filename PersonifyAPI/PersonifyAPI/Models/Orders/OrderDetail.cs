﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonifyAPI.Models.Orders
{
    public class OrderDetail
    {
        public string Code { get; set; }
        public string RateCode { get; set; }
        public string OrderNumber { get; set; }
        public int LineNumber { get; set; }
        public decimal AmountPaid { get; set; }
        public int Quantity { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}