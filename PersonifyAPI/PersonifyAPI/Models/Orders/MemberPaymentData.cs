﻿namespace PersonifyAPI.Models.Orders
{
    public class MemberPaymentData
    {
        public string MemberId { get; set; }
        public string ProductCode { get; set; }
        public string OrderNumber { get; set; }
        public int LineNum { get; set; }
        public CreditCardInfo CreditCard { get; set; }
        public Amount Amount { get; set; }
    }
}