﻿using System.ComponentModel.DataAnnotations;

namespace PersonifyAPI.Models.Orders
{
    public class PaymentRequest
    {
        public string MemberMasterCustomerId { get; set; }

        public string ProductCode { get; set; }

        public string OrderNumber { get; set; }

        [Required]
        public CreditCardInfo CreditCard { get; set; }

        [Required]
        public AmountInfo Amount { get; set; }

        private static string MaskCreditCardNumber(string number)
        {
            if (string.IsNullOrEmpty(number))
            {
                return string.Empty;
            }

            number = number.Replace(" ", "");

            if (number.Length != 16)
            {
                return number;
            }

            return number.Substring(0, 6)
                   + "** **** "
                   + number.Substring(12, 4);
        }
    }
}